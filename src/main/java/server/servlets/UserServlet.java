package server.servlets;

import data.JSONConverter;
import data.dao.UserDAO;
import data.dao.UsersAuthorisedDAO;
import data.dao.jdbc.UserDAOImpl;
import data.dao.jdbc.UsersAuthorisedDAOImpl;
import data.entities.Result;
import data.entities.User;
import data.entities.UsersAuthorised;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet(name = "UserServlet")
public class UserServlet extends HttpServlet {

    private final int WRONG_PASSWORD = 3;
    private final int EMAIL_TAKEN = 4;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json;charset=utf8");

        int code = updateByParameters(request.getParameterMap());

        Result result = new Result();

        switch (code) {
            case Result.OPERATION_SUCCESSFUL:
            result.setMessage("Данные успешно обновлены!");
            result.setCode(Result.OPERATION_SUCCESSFUL);
            break;
            case WRONG_PASSWORD:
            result.setMessage("Неверный пароль!");
            result.setCode(WRONG_PASSWORD);
            break;
            case EMAIL_TAKEN:
            result.setMessage("Такой адрес электронной почты уже занят!");
            result.setCode(EMAIL_TAKEN);
            break;
            default:
            result.setMessage("Не удалось обновить данные.");
            result.setCode(Result.OPERATION_FAILED);
        }

       response.getWriter().append(JSONConverter.toJSON(Result.BASE_NAME,result));

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("application/json;charset=utf8");

        UserDAO userDAO = new UserDAOImpl();

        boolean isDeleted = userDAO.delete(Integer.parseInt(req.getParameter("id")));

        Result result = new Result();

        if(isDeleted) {
            result.setCode(Result.OPERATION_SUCCESSFUL);
            result.setMessage("Учётная запись удалена!");
        }else{
            result.setCode(Result.OPERATION_FAILED);
            result.setMessage("Не удалось удалить учётную запись.");
        }

        resp.getWriter().append(JSONConverter.toJSON(Result.BASE_NAME,result));

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("application/json;charset=utf8");

        UserDAO userDAO = new UserDAOImpl();

        List<User> alUsers = userDAO.find(req.getParameterMap());

        resp.getWriter().append(JSONConverter.toJSON(User.BASE_NAME,alUsers));

    }

    private int updateByParameters(Map<String, String[]> parameterMap){

        UserDAO userDAO = new UserDAOImpl();

        User user = new User();

        Map<String, String[]> mParameters;

        if(parameterMap.containsKey("id") && parameterMap.get("id") != null && !parameterMap.get("id")[0].isEmpty()){

            user.setId(Integer.parseInt(parameterMap.get("id")[0]));

            if(parameterMap.containsKey("new_password")) {
                if (parameterMap.containsKey("old_password")) {

                    mParameters = new HashMap<>();

                    mParameters.put("password", parameterMap.get("old_password"));
                    mParameters.put("id", parameterMap.get("id"));

                    List<User> alUsers = userDAO.find(mParameters);

                    if(!alUsers.isEmpty()) user.setPassword(parameterMap.get("new_password")[0]);
                    else return WRONG_PASSWORD;

                    UsersAuthorisedDAO usersAuthorisedDAO = new UsersAuthorisedDAOImpl();

                    usersAuthorisedDAO.update(new UsersAuthorised(Integer.parseInt(parameterMap.get("id")[0]),"",0));

                }else{
                    return Result.OPERATION_FAILED;
                }
            }


            if (parameterMap.containsKey("email")){

                mParameters = new HashMap<>();
                mParameters.put("email", parameterMap.get("email"));

                if(userDAO.find(mParameters).isEmpty()) {
                    user.setEmail(parameterMap.get("email")[0]);
                }else return EMAIL_TAKEN;
            }

            if(parameterMap.containsKey("first_name")){
                user.setFirstName(parameterMap.get("first_name")[0]);
            }
            if(parameterMap.containsKey("last_name")){
                user.setLastName(parameterMap.get("last_name")[0]);
            }

        }else{
            return Result.OPERATION_FAILED;
        }

        boolean isUpdated = userDAO.update(user);

        if(isUpdated) return Result.OPERATION_SUCCESSFUL;
        else return Result.OPERATION_FAILED;

    }

}
