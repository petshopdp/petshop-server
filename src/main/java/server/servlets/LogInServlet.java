package server.servlets;

import data.JSONConverter;
import data.dao.UserDAO;
import data.dao.UsersAuthorisedDAO;
import data.dao.jdbc.UserDAOImpl;
import data.dao.jdbc.UsersAuthorisedDAOImpl;
import data.entities.User;
import data.entities.UsersAuthorised;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet(name = "LogInServlet")
public class LogInServlet extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json;charset=utf8");

        User user;

        UserDAO userDAO = new UserDAOImpl();

        UsersAuthorisedDAO usersAuthorisedDAO = new UsersAuthorisedDAOImpl();

        if(request.getParameterMap().size() == 1 && request.getParameter("device_tag")!=null){

            Map<String, String[]> parameterMap = new HashMap<>();

            parameterMap.put("device_tag", request.getParameterMap().get("device_tag"));
            parameterMap.put("is_authorised", new String[]{"1"});

            List<UsersAuthorised> alUsersAuthorised = usersAuthorisedDAO.find(parameterMap);

            if(alUsersAuthorised!=null && !alUsersAuthorised.isEmpty()) {
            user = userDAO.read(alUsersAuthorised.get(0).getUserID());
            response.getWriter().append(JSONConverter.toJSON(User.BASE_NAME,user));
            return;
            }else{
            user = new User(0,"","","","","");
            response.getWriter().append(JSONConverter.toJSON(User.BASE_NAME,user));
            return;
            }
        }

        List<User> alUser = userDAO.find(request.getParameterMap());

        if(!alUser.isEmpty()) {

            UsersAuthorised usersAuthorised = new UsersAuthorised();

            usersAuthorised.setUserID(alUser.get(0).getId());
            usersAuthorised.setDeviceTag(request.getParameter("device_tag"));
            usersAuthorised.setAuthorised(1);

            boolean authorised = usersAuthorisedDAO.update(usersAuthorised);

            if(!authorised) usersAuthorisedDAO.create(usersAuthorised);

            user = alUser.get(0);

        }else{
            user = new User(0,"","","","","");
        }

        response.getWriter().append(JSONConverter.toJSON(User.BASE_NAME,user));

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}



}
