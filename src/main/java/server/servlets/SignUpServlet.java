package server.servlets;

import data.JSONConverter;
import data.dao.UserDAO;
import data.dao.jdbc.UserDAOImpl;
import data.entities.Result;
import data.entities.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet(name = "SignUpServlet")

public class SignUpServlet extends HttpServlet {

    private final int USERNAME_TAKEN = 2;
    private final int EMAIL_TAKEN = 3;
    private final int USERNAME_AND_EMAIL_TAKEN = 4;
    private final int SERVER_ERROR = 5;


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json;charset=utf8");

        UserDAO userDAO = new UserDAOImpl();

        Map<String, String[]> map = new HashMap<>();

        map.put("username",request.getParameterMap().get("username"));
        map.put("email", request.getParameterMap().get("email"));

        List alUsers = userDAO.find(map);

        Result result = new Result();

        if(!alUsers.isEmpty()){
            result.setMessage("Имя пользователя \'" + request.getParameter("username") + "\' и адрес эл. почты \'"
                              + request.getParameter("email") + "\' уже заняты!");
            result.setCode(USERNAME_AND_EMAIL_TAKEN);
            response.getWriter().append(JSONConverter.toJSON(Result.BASE_NAME,result));
            return;
        }

        map.clear();
        map.put("username",request.getParameterMap().get("username"));

        if(!alUsers.isEmpty()){
            result.setMessage("Имя пользователя \'" + request.getParameter("username") + "\' уже занято!");
            result.setCode(USERNAME_TAKEN);
            response.getWriter().append(JSONConverter.toJSON(Result.BASE_NAME,result));
            return;
        }

        map.clear();
        map.put("email", request.getParameterMap().get("email"));

        alUsers = userDAO.find(map);

        if(!alUsers.isEmpty()){
          result.setMessage("Адрес электронной почты \'" + request.getParameter("email") + "\' уже используется одиним из пользовтелей");
          result.setCode(EMAIL_TAKEN);
          response.getWriter().append(JSONConverter.toJSON(Result.BASE_NAME,result));
          return;
        }

        User user = new User();

        user.setUsername(request.getParameter("username"));
        user.setPassword(request.getParameter("password"));
        user.setFirstName(request.getParameter("first_name"));
        user.setLastName(request.getParameter("last_name"));
        user.setEmail(request.getParameter("email"));

        boolean isCreated = userDAO.create(user);

        if(isCreated) {
            result.setCode(Result.OPERATION_SUCCESSFUL);
            result.setMessage("Регистрация прошла успешно!");
        }else{
            result.setCode(SERVER_ERROR);
            result.setMessage("Произошла ошибка на сервере, приносим свои извинения");
        }

        response.getWriter().append(JSONConverter.toJSON(Result.BASE_NAME,result));

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
