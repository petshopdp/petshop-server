package server.servlets;

import data.JSONConverter;
import data.dao.BrandDAO;
import data.dao.jdbc.BrandDAOImpl;
import data.entities.Brand;
import data.entities.Type;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "BrandServlet")
public class BrandServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json;charset=utf8");

        BrandDAO brandDAO = new BrandDAOImpl();

        List<Brand> alBrands = brandDAO.find(request.getParameterMap());

        response.getWriter().append(JSONConverter.toJSON(Brand.BASE_NAME,alBrands));


    }
}
