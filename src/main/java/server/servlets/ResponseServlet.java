package server.servlets;

import data.JSONConverter;
import data.dao.CommentResponseDAO;
import data.dao.jdbc.CommentResponseDAOImpl;
import data.entities.CommentResponse;
import data.entities.Result;
import data.entities.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ResponseServlet")
public class ResponseServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json;charset=utf8");

        CommentResponseDAO responseDAO = new CommentResponseDAOImpl();

        Result result = new Result();

        if(request.getParameter("id")!=null){

            boolean isUpdated = responseDAO.update(Integer.parseInt(request.getParameter("id")),request.getParameter("response_text"));

            if(isUpdated){
                result.setCode(Result.OPERATION_SUCCESSFUL);
                result.setMessage("Ответ успешно изменён!");
            }else{
                result.setCode(Result.OPERATION_FAILED);
                result.setMessage("Не удалось изменить ответ.");
            }

            response.getWriter().append(JSONConverter.toJSON(Result.BASE_NAME,result));

            return;

        }

        CommentResponse commentResponse = new CommentResponse();

        commentResponse.setResponseText(request.getParameter("response_text"));

        User user = new User();
        user.setId(Integer.parseInt(request.getParameter("user_id")));

        commentResponse.setUser(user);
        commentResponse.setCommentId(Integer.parseInt(request.getParameter("comment_id")));

        boolean isCreated = responseDAO.create(commentResponse);

        if(isCreated){
            result.setCode(Result.OPERATION_SUCCESSFUL);
            result.setMessage("Ответ успешно добавлен!");
        }else{
            result.setCode(Result.OPERATION_FAILED);
            result.setMessage("Не удалить ответ.");
        }

        response.getWriter().append(JSONConverter.toJSON(Result.BASE_NAME,result));

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json;charset=utf8");

        CommentResponseDAO responseDAO = new CommentResponseDAOImpl();

        List<CommentResponse> alResponses = responseDAO.find(request.getParameterMap());

        response.getWriter().append(JSONConverter.toJSON(CommentResponse.BASE_NAME,alResponses));

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("application/json;charset=utf8");

        CommentResponseDAO responseDAO = new CommentResponseDAOImpl();

        boolean isDeleted = responseDAO.delete(Integer.parseInt(req.getParameter("id")));

        Result result = new Result();

        if(isDeleted){
            result.setCode(Result.OPERATION_SUCCESSFUL);
            result.setMessage("Ответ успешно удалён!");
        }else{
            result.setCode(Result.OPERATION_FAILED);
            result.setMessage("Не удалось удалить ответ.");
        }

        resp.getWriter().append(JSONConverter.toJSON(Result.BASE_NAME,result));

    }
}
