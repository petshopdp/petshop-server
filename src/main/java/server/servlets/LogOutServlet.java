package server.servlets;

import data.JSONConverter;
import data.dao.UsersAuthorisedDAO;
import data.dao.jdbc.UsersAuthorisedDAOImpl;
import data.entities.Result;
import data.entities.UsersAuthorised;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "LogOutServlet")
public class LogOutServlet extends HttpServlet {

    private final int OPERATION_SUCCESSFUL = 1;
    private final int OPERATION_FAILED = 2;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json;charset=utf8");

        UsersAuthorised usersAuthorised = new UsersAuthorised();

        usersAuthorised.setUserID(Integer.parseInt(request.getParameter("user_id")));
        usersAuthorised.setDeviceTag(request.getParameter("device_tag"));
        usersAuthorised.setAuthorised(0);

        UsersAuthorisedDAO usersAuthorisedDAO = new UsersAuthorisedDAOImpl();

        boolean loggedOut = usersAuthorisedDAO.update(usersAuthorised);

        Result result = new Result();

        if(loggedOut){
            result.setCode(OPERATION_SUCCESSFUL);
            result.setMessage("Успешный выход из учётной записи!");
        }else{
            result.setCode(OPERATION_FAILED);
            result.setMessage("Не удалось выйти из учётный записи.");
        }

        response.getWriter().append(JSONConverter.toJSON(Result.BASE_NAME,result));

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
