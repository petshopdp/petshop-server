package server.servlets;

import data.JSONConverter;
import data.dao.CommentDAO;
import data.dao.jdbc.CommentDAOImpl;
import data.entities.Comment;
import data.entities.Result;
import data.entities.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "CommentServlet")
public class CommentServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json;charset=utf8");

        CommentDAO commentDAO = new CommentDAOImpl();

        Result result = new Result();

        if(request.getParameter("id")!=null){

        boolean isUpdated = commentDAO.update(Integer.parseInt(request.getParameter("id")),request.getParameter("comment_text"));

        if(isUpdated){
            result.setCode(Result.OPERATION_SUCCESSFUL);
            result.setMessage("Комментарий успешно изменён!");
        }else{
            result.setCode(Result.OPERATION_FAILED);
            result.setMessage("Не удалось изменить комментарий");
        }

        response.getWriter().append(JSONConverter.toJSON(Result.BASE_NAME,result));

        return;

        }

        Comment comment = new Comment();

        comment.setCommentText(request.getParameter("comment_text"));

        User user = new User();
        user.setId(Integer.parseInt(request.getParameter("user_id")));

        comment.setUser(user);
        comment.setGoodID(Integer.parseInt(request.getParameter("good_id")));

        boolean isCreated = commentDAO.create(comment);

        if(isCreated){
            result.setCode(Result.OPERATION_SUCCESSFUL);
            result.setMessage("Комментарий успешно добавлен!");
        }else{
            result.setCode(Result.OPERATION_FAILED);
            result.setMessage("Не удалось добавить комментарий.");
        }

        response.getWriter().append(JSONConverter.toJSON(Result.BASE_NAME,result));

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json;charset=utf8");

        CommentDAO commentDAO = new CommentDAOImpl();

        List<Comment> alComments = commentDAO.find(request.getParameterMap());

        response.getWriter().append(JSONConverter.toJSON(Comment.BASE_NAME,alComments));

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("application/json;charset=utf8");

        CommentDAO commentDAO = new CommentDAOImpl();

        boolean isDeleted = commentDAO.delete(Integer.parseInt(req.getParameter("id")));

        Result result = new Result();

        if(isDeleted){
            result.setCode(Result.OPERATION_SUCCESSFUL);
            result.setMessage("Комментарий успешно удалён!");
        }else{
            result.setCode(Result.OPERATION_FAILED);
            result.setMessage("Не удалось удалить комментарий.");
        }

        resp.getWriter().append(JSONConverter.toJSON(Result.BASE_NAME,result));

    }
}
