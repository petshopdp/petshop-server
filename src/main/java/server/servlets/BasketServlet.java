package server.servlets;

import data.JSONConverter;
import data.dao.BasketDAO;
import data.dao.jdbc.BasketDAOImpl;
import data.entities.BasketItem;
import data.entities.Result;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "BasketServlet")
public class BasketServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json;charset=utf8");

        BasketDAO basketDAO = new BasketDAOImpl();

        BasketItem basketItem = new BasketItem();

        basketItem.setUserID(Integer.parseInt(request.getParameter("user_id")));
        basketItem.setGoodID(Integer.parseInt(request.getParameter("good_id")));
        basketItem.setCount(Integer.parseInt(request.getParameter("count")));

        boolean isCreated = basketDAO.create(basketItem);

        Result result = new Result();

        if(isCreated){
            result.setCode(Result.OPERATION_SUCCESSFUL);
            result.setMessage("Товар успешно добавлен в корзину.");
        }else{
            result.setCode(Result.OPERATION_FAILED);
            result.setMessage("Произошла ошибка при добавлении товара в корзину.");
        }

        response.getWriter().append(JSONConverter.toJSON(Result.BASE_NAME,result));

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json;charset=utf8");

        BasketDAO basketDAO = new BasketDAOImpl();

        List<BasketItem> alItems = basketDAO.find(Integer.parseInt(request.getParameter("user_id")));

        response.getWriter().append(JSONConverter.toJSON(BasketItem.BASE_NAME,alItems));

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("application/json;charset=utf8");

        BasketDAO basketDAO = new BasketDAOImpl();

        boolean isDeleted = basketDAO.delete(Integer.parseInt(req.getParameter("good_id")),
                                                              Integer.parseInt(req.getParameter("user_id")));

        Result result = new Result();

        if(isDeleted){
            result.setCode(Result.OPERATION_SUCCESSFUL);
            result.setMessage("Товар успешно удалён из корзины.");
        }else{
            result.setCode(Result.OPERATION_FAILED);
            result.setMessage("Произошла ошибка при попытке удаления товара из корзины.");
        }

        resp.getWriter().append(JSONConverter.toJSON(Result.BASE_NAME,result));

    }
}
