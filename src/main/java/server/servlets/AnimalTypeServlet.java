package server.servlets;

import data.JSONConverter;
import data.dao.AnimalTypeDAO;
import data.dao.jdbc.AnimalTypeDAOImpl;
import data.entities.AnimalType;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "AnimalTypeReqServlet")
public class AnimalTypeServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        response.setContentType("application/json;charset=utf8");

        AnimalTypeDAO animalTypeDAO = new AnimalTypeDAOImpl();

        List<AnimalType> alAnimalTypes = animalTypeDAO.find(request.getParameterMap());

        response.getWriter().append(JSONConverter.toJSON(AnimalType.BASE_NAME,alAnimalTypes));

    }

}
