package server.servlets;

import data.JSONConverter;
import data.dao.jdbc.GoodDAOImpl;
import data.dao.GoodDAO;
import data.entities.Good;

import java.io.IOException;
import java.util.List;

public class GoodServlet extends javax.servlet.http.HttpServlet {


    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

        response.setContentType("application/json;charset=utf8");

        GoodDAO goodDAO = new GoodDAOImpl();

        List<Good> alGood = goodDAO.find(request.getParameterMap());

        response.getWriter().append(JSONConverter.toJSON(Good.BASE_NAME,alGood));

    }
}
