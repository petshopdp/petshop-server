package server.servlets;

import data.JSONConverter;
import data.dao.jdbc.CategoryDAOImpl;
import data.dao.CategoryDAO;
import data.entities.Category;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "CategoryServlet")
public class CategoryServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json;charset=utf8");

        CategoryDAO categoryDAO = new CategoryDAOImpl();

        List<Category> alCategory = categoryDAO.find(request.getParameterMap());

        response.getWriter().append(JSONConverter.toJSON(Category.BASE_NAME,alCategory));

    }

}
