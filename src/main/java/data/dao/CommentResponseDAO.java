package data.dao;

import data.entities.CommentResponse;

import java.util.List;
import java.util.Map;

public interface CommentResponseDAO {
    boolean create(CommentResponse commentResponse);

    CommentResponse read(int id);

    boolean update(int id, String responseText);

    boolean delete(int id);

    List<CommentResponse> find(Map<String, String[]> parameterMap);
}
