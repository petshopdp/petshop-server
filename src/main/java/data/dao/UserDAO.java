package data.dao;

import data.entities.User;

import java.util.List;
import java.util.Map;

public interface UserDAO {

    boolean create(User user);

    User read(int id);

    boolean update(User user);

    boolean delete(int id);

    List<User> find(Map<String, String[]> parameterMap);

}
