package data.dao;

import data.entities.Type;

import java.util.List;
import java.util.Map;

public interface TypeDAO {

    boolean create(Type type);
    Type read(int id);
    List<Type> find(Map<String, String[]> parameterMap);
    boolean update(Type type);
    boolean delete(int id);

}
