package data.dao;

import data.entities.UsersAuthorised;

import java.util.List;
import java.util.Map;

public interface UsersAuthorisedDAO {

    boolean create(UsersAuthorised usersAuthorised);
    UsersAuthorised read(int user_id, String deviceTag);
    boolean update(UsersAuthorised usersAuthorised);
    List<UsersAuthorised> find(Map<String, String[]> parameterMap);
    boolean delete(int userID);

}
