package data.dao;

import data.entities.BasketItem;

import java.util.List;

public interface BasketDAO {
    List<BasketItem> find(int userID);

    boolean create(BasketItem basketItem);

    BasketItem read(int goodID, int userID);

    boolean update(BasketItem basketItem);

    boolean delete(int goodID, int userID);
}
