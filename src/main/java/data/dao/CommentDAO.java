package data.dao;

import data.entities.Comment;

import java.util.List;
import java.util.Map;

public interface CommentDAO {
    boolean create(Comment comment);

    Comment read(int id);

    boolean update(int id, String commentText);

    boolean delete(int id);

    List<Comment> find(Map<String, String[]> parameterMap);
}
