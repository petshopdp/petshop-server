package data.dao;

import data.entities.Good;

import java.util.List;
import java.util.Map;

public interface GoodDAO {

    boolean create(Good good);

    Good read(int id);

    List<Good> find(Map<String, String[]> parameterMap);

    boolean update(Good good);

    boolean delete(int id);
}
