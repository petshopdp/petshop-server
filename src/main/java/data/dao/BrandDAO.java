package data.dao;

import data.entities.Brand;

import java.util.List;
import java.util.Map;

public interface BrandDAO {

    boolean create(Brand brand);
    Brand read(int id);
    List<Brand> find(Map<String, String[]> parameterMap);
    boolean update(Brand brand);
    boolean delete(int id);

}
