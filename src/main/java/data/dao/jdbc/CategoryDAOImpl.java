package data.dao.jdbc;

import data.dao.CategoryDAO;
import data.entities.Category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CategoryDAOImpl extends AbstractDAO<Category> implements CategoryDAO {

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM categories ";
    }

    @Override
    protected String getUpdateQuery(Category entity) {

        String sql = "UPDATE categories SET";
        boolean isMoreThanOneFieldToUpdate = false;

        if(entity!=null && entity.getId() != 0) {
            sql += " id=\'" + entity.getId() + "\'";
            isMoreThanOneFieldToUpdate = true;
        }

        if(entity!=null && !entity.getName().isEmpty()){
            sql+=(isMoreThanOneFieldToUpdate)?",":"";
            sql+=" name=\'" + entity.getName() + "\'";
            isMoreThanOneFieldToUpdate = true;
        }

        if(entity!=null && !entity.getUrl().isEmpty()){
            sql+=(isMoreThanOneFieldToUpdate)?",":"";
            sql+=" url=\'" + entity.getUrl() + "\'";
        }

        return sql;

    }

    @Override
    protected String getSelectForAllQuery() {
        return "SELECT * FROM categories";
    }

    @Override
    protected int getID(Category entity) {
        return entity.getId();
    }

    @Override
    protected PreparedStatement getPreparedCreateStatement(Connection connection, Category entity) throws SQLException {

        PreparedStatement psCategory = connection.prepareStatement("INSERT INTO categories(`name`, url) VALUES (?,?)");

        psCategory.setString(1,entity.getName());
        psCategory.setString(2,entity.getUrl());

        return psCategory;

    }

    @Override
    protected String getSelectQuery(Map<String, String[]> parameterMap) {

        String sql = "SELECT * FROM categories ";
        boolean isMoreThanOneCondition = false;

        if(!parameterMap.isEmpty()) {

            sql+="WHERE";

            if (parameterMap.containsKey("id")) {
               sql+=" id=" + parameterMap.get("id")[0];
               isMoreThanOneCondition = true;
            }
            if (parameterMap.containsKey("name")) {
                sql+=(isMoreThanOneCondition)?" AND":"";
                sql+=" name=\'" + parameterMap.get("name")[0] + "\'";
                isMoreThanOneCondition = true;
            }
            if(parameterMap.containsKey("url")) {
                sql+=(isMoreThanOneCondition)?" AND":"";
                sql+=" url=\'" + parameterMap.get("url")[0] + "\'";
                isMoreThanOneCondition = true;
            }
            if(parameterMap.containsKey("animal_type_id")){
                sql+=(isMoreThanOneCondition)?" AND":"";
                sql+=" id IN (SELECT category_id FROM categories_for_animal_types WHERE animal_type_id="
                     + parameterMap.get("animal_type_id")[0] + ")";
            }
            if(parameterMap.containsKey("brand_id")){
                sql+=(isMoreThanOneCondition)?" AND":"";
                sql+=" id IN (SELECT category_id FROM goods WHERE brand_id="
                    + parameterMap.get("brand_id")[0] + ")";
            }
        }

        sql+=";";

        return sql;

    }

    @Override
    protected List<Category> toEntity(ResultSet rs) throws SQLException {

        ArrayList<Category> alCategories = new ArrayList<>();

        Category category;

        while(rs.next()){

            category = new Category();

            category.setId(rs.getInt("id"));
            category.setName(rs.getString("name"));
            category.setUrl(rs.getString("url"));

            alCategories.add(category);

        }

        return alCategories;

    }

}
