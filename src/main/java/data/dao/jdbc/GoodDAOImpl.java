package data.dao.jdbc;

import data.dao.GoodDAO;
import data.entities.*;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GoodDAOImpl implements GoodDAO {

    private Logger logger = Logger.getLogger(GoodDAOImpl.class);

    private final byte FOOD = 1;
    private final byte CARE = 2;
    private final byte CAGES = 3;
    private final byte TOYS = 4;
    private final byte FILLERS = 5;
    private final byte ANIMALS = 6;


    @Override
    public boolean create(Good good){

        Connection connection = null;

        try{
            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psGood = null;

            if(good instanceof Food){
                psGood = connection.prepareStatement("INSERT INTO goods(`name`,brand_id, type_id, animal_type_id," +
                                                          "weight, description, `count`, price, url, category_id) VALUES (?,?,?,?,?,?,?,?,?,?)");
                psGood.setString(1,good.getName());
                psGood.setInt(2,((Food) good).getBrand().getId());
                psGood.setInt(3,good.getType().getId());
                psGood.setInt(4,((Food)good).getAnimalType().getId());
                psGood.setInt(5,((Food) good).getWeight());
                psGood.setString(6,good.getDescription());
                psGood.setInt(7, good.getCount());
                psGood.setDouble(8,good.getPrice());
                psGood.setString(9,good.getUrl());
                psGood.setInt(10,good.getCategoryID());
            }else if(good instanceof Care){
                psGood = connection.prepareStatement("INSERT INTO goods(`name`,brand_id, type_id, animal_type_id," +
                        "description, `count`, price, url, category_id) VALUES (?,?,?,?,?,?,?,?,?)");
                psGood.setString(1,good.getName());
                psGood.setInt(2,((Care) good).getBrand().getId());
                psGood.setInt(3,good.getType().getId());
                psGood.setInt(4,((Care)good).getAnimalType().getId());
                psGood.setString(5,good.getDescription());
                psGood.setInt(6, good.getCount());
                psGood.setDouble(7,good.getPrice());
                psGood.setString(8,good.getUrl());
                psGood.setInt(9,good.getCategoryID());
            }else if(good instanceof Cage){
                psGood = connection.prepareStatement("INSERT INTO goods(`name`,brand_id, type_id, animal_type_id," +
                                                          "`length`,width,height,description, `count`, price, url,category_id) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
                psGood.setString(1,good.getName());
                psGood.setInt(2,((Cage) good).getBrand().getId());
                psGood.setInt(3,good.getType().getId());
                psGood.setInt(4, ((Cage) good).getLength());
                psGood.setInt(5,((Cage) good).getWidth());
                psGood.setInt(6, ((Cage) good).getHeight());
                psGood.setString(7,good.getDescription());
                psGood.setInt(8, good.getCount());
                psGood.setDouble(9,good.getPrice());
                psGood.setString(10,good.getUrl());
                psGood.setInt(11,good.getCategoryID());
            }else if(good instanceof Toy){
                psGood = connection.prepareStatement("INSERT INTO goods(`name`,brand_id, type_id, animal_type_id," +
                                                          "description, `count`, price, url, category_id) VALUES (?,?,?,?,?,?,?,?,?)");
                psGood.setString(1,good.getName());
                psGood.setInt(2,((Toy) good).getBrand().getId());
                psGood.setInt(3,good.getType().getId());
                psGood.setInt(4,((Toy)good).getAnimalType().getId());
                psGood.setString(5,good.getDescription());
                psGood.setInt(6, good.getCount());
                psGood.setDouble(7,good.getPrice());
                psGood.setString(8,good.getUrl());
                psGood.setInt(9,good.getCategoryID());
            }else if(good instanceof Filler){
                psGood = connection.prepareStatement("INSERT INTO goods(`name`,brand_id, type_id, animal_type_id," +
                        "weight, description, `count`, price, url, category_id) VALUES (?,?,?,?,?,?,?,?,?,?)");
                psGood.setString(1,good.getName());
                psGood.setInt(2,((Filler) good).getBrand().getId());
                psGood.setInt(3,good.getType().getId());
                psGood.setInt(4,((Filler)good).getAnimalType().getId());
                psGood.setInt(5,((Filler) good).getWeight());
                psGood.setString(6,good.getDescription());
                psGood.setInt(7, good.getCount());
                psGood.setDouble(8,good.getPrice());
                psGood.setString(9,good.getUrl());
                psGood.setInt(10,good.getCategoryID());
            }else if(good instanceof Animal){
                psGood = connection.prepareStatement("INSERT INTO goods(`name`, type_id,sex,age,breed_id," +
                                                          "description, `count`, price, url, category_id) VALUES (?,?,?,?,?,?,?,?,?,?)");
                psGood.setString(1, good.getName());
                psGood.setInt(2, good.getType().getId());
                psGood.setString(3, String.valueOf(((Animal) good).getSex()));
                psGood.setInt(4, ((Animal) good).getAge());
                psGood.setInt(5, ((Animal) good).getBreed().getId());
                psGood.setString(6, good.getDescription());
                psGood.setInt(7, good.getCount());
                psGood.setDouble(8, good.getPrice());
                psGood.setString(9, good.getUrl());
                psGood.setInt(10,good.getCategoryID());
            }

            psGood.executeUpdate();

            return true;

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(), e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }

        return false;
    }

    private List<Good> getAllGoods(){

        Connection connection = null;

        try{
            connection = DBConnectionManager.getInstance().getConnection();
            PreparedStatement psAllGoods = connection.prepareStatement("SELECT g.id, g.name, g.brand_id, b.name as brand," +
                    "g.category_id, g.type_id, t.name as `type`," +
                    "g.description, g.count, g.price, g.url FROM goods g " +
                    "LEFT JOIN brands b ON g.brand_id = b.id " +
                    "LEFT JOIN categories cat ON g.category_id = cat.id " +
                    "LEFT JOIN types t ON  g.type_id = t.id;");
            ResultSet rsAllGoods = psAllGoods.executeQuery();

            return toEntity(rsAllGoods,"0");

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e) {
            logger.error(e.getMessage(), e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return null;
    }

    @Override
    public Good read(int id){

        Connection connection = null;

        try{
            connection = DBConnectionManager.getInstance().getConnection();

            String categoryID = getCategory(String.valueOf(id));

            PreparedStatement psGood = connection.prepareStatement(getGoodStatementBasedOnCategory(
                                                                    categoryID," WHERE id=" + id + ";"));

            ResultSet rsGood = psGood.executeQuery();

            return toEntity(rsGood, categoryID).get(0);

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(), e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return null;
    }

    private String getCategory(String id){

        Connection connection = null;

        try{

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psCategory = connection.prepareStatement("SELECT category_id FROM goods WHERE id=?");
            psCategory.setString(1,id);

            ResultSet rsCategory = psCategory.executeQuery();

            if(rsCategory.next()) return rsCategory.getString(1);
            else return null;

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(), e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return null;
    }

    @Override
    public List<Good> find(Map<String, String[]> parameterMap){

        String categoryID;

        Connection connection = null;

        try{

            connection = DBConnectionManager.getInstance().getConnection();

            if(parameterMap.isEmpty()){
               return getAllGoods();
            }

            if (parameterMap.containsKey("category_id") && parameterMap.get("category_id")[0] != null && !parameterMap.get("category_id")[0].isEmpty()) {
                categoryID = parameterMap.get("category_id")[0];
            } else {
                if (parameterMap.containsKey("id") && parameterMap.get("id")[0] != null && !parameterMap.get("id")[0].isEmpty()) {
                        String categoryResult = getCategory(parameterMap.get("id")[0]);
                        if(categoryResult!=null)categoryID = categoryResult;
                        else return null;
                } else if (parameterMap.containsKey("breed_id")) {
                    categoryID = String.valueOf(ANIMALS);
                } else {
                    return null;
                }
            }

            PreparedStatement psGoods = connection.prepareStatement(getGoodStatementBasedOnCategory(categoryID, getSqlQuery(parameterMap)));

            ResultSet rsGoods = psGoods.executeQuery();

            return toEntity(rsGoods, categoryID);

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(), e);
        }finally {
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return null;
    }

    @Override
    public boolean update(Good good){

        Connection connection = null;

        try{

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psGood = connection.prepareStatement(getUpdateQuery(good));

            psGood.executeUpdate();

            return true;

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e) {
            logger.error(e.getMessage(), e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return false;
    }

    @Override
    public boolean delete(int id){

        Connection connection = null;

        try {
            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psGood = connection.prepareStatement("DELETE FROM goods WHERE id=?;");
            psGood.setInt(1,id);

            int rowsAffected = psGood.executeUpdate();

            return rowsAffected > 0;

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
          logger.error(e.getMessage(), e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return false;
    }

    private List<Good> toEntity(ResultSet rs, String categoryID) throws SQLException {

        ArrayList<Good> alGoods = new ArrayList<>();

        Good good;

        while(rs.next()) {

            switch(Integer.parseInt(categoryID)){
                case FOOD:
                    good = new Food();
                    break;
                case CARE:
                    good = new Care();
                    break;
                case CAGES:
                    good = new Cage();
                    break;
                case TOYS:
                    good = new Toy();
                    break;
                case FILLERS:
                    good = new Filler();
                    break;
                case ANIMALS:
                    good = new Animal();
                    break;
                default:
                    good = new Good();
            }

            good.setId(rs.getInt("id"));
            good.setName(rs.getString("name"));
            good.setCategoryID(rs.getInt("category_id"));
            good.setType(new Type(rs.getInt("type_id"), rs.getString("type"),null));
            good.setDescription(rs.getString("description"));
            good.setCount(rs.getInt("count"));
            good.setPrice(rs.getDouble("price"));
            good.setUrl(rs.getString("url"));

            switch (Integer.parseInt(categoryID)) {
                case FOOD:
                ((Food) good).setBrand(new Brand(rs.getInt("brand_id"), rs.getString("brand")));
                ((Food) good).setAnimalType(new AnimalType(rs.getInt("animal_type_id"), rs.getString("animal_type")));
                ((Food) good).setWeight(rs.getInt("weight"));
                break;
                case CARE:
                ((Care) good).setBrand(new Brand(rs.getInt("brand_id"), rs.getString("brand")));
                ((Care) good).setAnimalType(new AnimalType(rs.getInt("animal_type_id"), rs.getString("animal_type")));
                break;
                case CAGES:
                ((Cage) good).setBrand(new Brand(rs.getInt("brand_id"), rs.getString("brand")));
                ((Cage) good).setLength(rs.getInt("length"));
                ((Cage) good).setWidth(rs.getInt("width"));
                ((Cage) good).setHeight(rs.getInt("height"));
                break;
                case TOYS:
                ((Toy) good).setBrand(new Brand(rs.getInt("brand_id"), rs.getString("brand")));
                ((Toy) good).setAnimalType(new AnimalType(rs.getInt("animal_type_id"), rs.getString("animal_type")));
                break;
                case FILLERS:
                ((Filler) good).setBrand(new Brand(rs.getInt("brand_id"), rs.getString("brand")));
                ((Filler) good).setAnimalType(new AnimalType(rs.getInt("animal_type_id"), rs.getString("animal_type")));
                ((Filler) good).setWeight(rs.getInt("weight"));
                break;
                case ANIMALS:
                ((Animal) good).setBreed(new Breed(rs.getInt("breed_id"), rs.getString("breed")));
                ((Animal) good).setSex(rs.getString("sex").charAt(0));
                ((Animal) good).setAge(rs.getInt("age"));
                break;
            }

            alGoods.add(good);

        }

        return alGoods;

    }

    private String getSqlQuery(Map<String, String[]> parameterMap){

        String sql;
        boolean moreThanOneCondition = false;

        sql = " WHERE";

        if (parameterMap.containsKey("id") && parameterMap.get("id")[0] != null && !parameterMap.get("id")[0].isEmpty()) {
            sql += " g.id=" + parameterMap.get("id")[0];
            moreThanOneCondition = true;
        }

        if(parameterMap.containsKey("name") && parameterMap.get("name")[0] != null && !parameterMap.get("name")[0].isEmpty()){
            sql += (moreThanOneCondition) ? " AND" : "";
            sql += " g.name=\'" + parameterMap.get("name")[0] + "\'";
            moreThanOneCondition = true;
        }

        if(parameterMap.containsKey("category_id") && parameterMap.get("category_id")[0] != null && !parameterMap.get("category_id")[0].isEmpty()){
            sql += " g.category_id=" + parameterMap.get("category_id")[0];
            moreThanOneCondition = true;
        }

        if (parameterMap.containsKey("type_id") && parameterMap.get("type_id")[0] != null && !parameterMap.get("type_id")[0].isEmpty()) {
            sql += (moreThanOneCondition) ? " AND" : "";
            sql += " g.type_id=" + parameterMap.get("type_id")[0];
            moreThanOneCondition = true;
        }

        if (parameterMap.containsKey("brand_id") && parameterMap.get("brand_id")[0] != null && !parameterMap.get("brand_id")[0].isEmpty()) {
            sql += (moreThanOneCondition) ? " AND" : "";
            sql += " g.brand_id=" + parameterMap.get("brand_id")[0];
            moreThanOneCondition = true;
        }

        if (parameterMap.containsKey("breed_id") && parameterMap.get("breed_id")[0] != null && !parameterMap.get("breed_id")[0].isEmpty()) {
            sql += (moreThanOneCondition) ? " AND" : "";
            sql += " g.breed_id=" + parameterMap.get("breed_id")[0];
            moreThanOneCondition = true;
        }

        if (parameterMap.containsKey("animal_type_id") && parameterMap.get("animal_type_id")[0] != null && !parameterMap.get("animal_type_id")[0].isEmpty()) {
            sql += (moreThanOneCondition) ? " AND" : "";
            sql += " g.animal_type_id=" + parameterMap.get("animal_type_id")[0];
            moreThanOneCondition = true;
        }

        if (parameterMap.containsKey("min_price") && parameterMap.get("min_price")[0] != null && !parameterMap.get("min_price")[0].isEmpty()) {
            if (parameterMap.containsKey("max_price") && parameterMap.get("max_price")[0] != null && !parameterMap.get("max_price")[0].isEmpty()) {
                sql += (moreThanOneCondition) ? " AND" : "";
                sql += " g.price BETWEEN " +
                        parameterMap.get("min_price")[0] + " AND " + parameterMap.get("max_price")[0];
            }else{
                sql += (moreThanOneCondition)?" AND":"";
                sql += " g.price >= " + parameterMap.get("min_price")[0];
            }
        }else if(parameterMap.containsKey("max_price") && parameterMap.get("max_price")[0] != null && !parameterMap.get("max_price")[0].isEmpty()){
            sql += (moreThanOneCondition)?" AND":"";
            sql += " g.price <= " + parameterMap.get("max_price")[0];
        }

        sql+=";";

        return sql;

    }

    private String getUpdateQuery(Good good){

       String sql = "UPDATE goods SET";
       boolean isMoreThanOneFieldToUpdate = false;

       if(good.getName()!=null && !good.getName().isEmpty()){
           sql+=" name=\'" + good.getName() + "\'";
           isMoreThanOneFieldToUpdate = true;
       }
       if(good.getCategoryID()!=0){
           sql+=(isMoreThanOneFieldToUpdate)?",":"";
           sql+=" category_id=" + good.getCategoryID();
           isMoreThanOneFieldToUpdate = true;
       }
       if(good.getType()!=null && good.getType().getId() != 0){
           sql+=(isMoreThanOneFieldToUpdate)?",":"";
           sql+=" type_id=" + good.getType().getId();
           isMoreThanOneFieldToUpdate = true;
       }
       if(good.getDescription()!=null && !good.getDescription().isEmpty()){
           sql+=(isMoreThanOneFieldToUpdate)?",":"";
           sql+=" description=\'" + good.getDescription() + "\'";
           isMoreThanOneFieldToUpdate = true;
       }
       if(good.getPrice()!= 0){
           sql+=(isMoreThanOneFieldToUpdate)?",":"";
           sql+=" price=" + good.getPrice();
           isMoreThanOneFieldToUpdate = true;
       }
       if(good.getUrl()!=null && !good.getUrl().isEmpty()){
           sql+=(isMoreThanOneFieldToUpdate)?",":"";
           sql+=" url=\'" + good.getUrl() + "\'";
           isMoreThanOneFieldToUpdate = true;
       }

       if(good instanceof Food){
           if(((Food) good).getWeight()!=0){
               sql+=(isMoreThanOneFieldToUpdate)?",":"";
               sql+=" weight=" + ((Food) good).getWeight();
               isMoreThanOneFieldToUpdate = true;
           }
           if(((Food) good).getBrand()!=null&&((Food) good).getBrand().getId()!=0){
               sql+=(isMoreThanOneFieldToUpdate)?",":"";
               sql+=" brand_id=" + ((Food) good).getBrand().getId();
               isMoreThanOneFieldToUpdate = true;
           }
           if(((Food) good).getAnimalType() != null && ((Food) good).getBrand().getId() != 0){
               sql+=(isMoreThanOneFieldToUpdate)?",":"";
               sql+=" animal_type_id=" + ((Food) good).getAnimalType().getId();
           }
       }else if(good instanceof Care){
           if(((Care) good).getBrand()!=null&&((Care) good).getBrand().getId()!=0){
               sql+=(isMoreThanOneFieldToUpdate)?",":"";
               sql+=" brand_id=" + ((Care) good).getBrand().getId();
               isMoreThanOneFieldToUpdate = true;
           }
           if(((Care) good).getAnimalType() != null && ((Care) good).getBrand().getId() != 0){
               sql+=(isMoreThanOneFieldToUpdate)?",":"";
               sql+=" animal_type_id=" + ((Care) good).getAnimalType().getId();
           }
       }else if(good instanceof Cage){
           if(((Cage) good).getBrand()!=null&&((Cage) good).getBrand().getId()!=0){
               sql+=(isMoreThanOneFieldToUpdate)?",":"";
               sql+=" brand_id=" + ((Cage) good).getBrand().getId();
               isMoreThanOneFieldToUpdate = true;
           }
           if(((Cage) good).getWidth() != 0){
               sql+=(isMoreThanOneFieldToUpdate)?",":"";
               sql+=" width=" + ((Cage) good).getWidth();
               isMoreThanOneFieldToUpdate = true;
           }
           if(((Cage) good).getHeight() != 0){
               sql+=(isMoreThanOneFieldToUpdate)?",":"";
               sql+=" height=" + ((Cage) good).getHeight();
               isMoreThanOneFieldToUpdate = true;
           }
           if(((Cage) good).getLength() != 0){
               sql+=(isMoreThanOneFieldToUpdate)?",":"";
               sql+=" length=" + ((Cage) good).getLength();
           }
       }else if(good instanceof Toy){
           if(((Toy) good).getBrand()!=null&&((Toy) good).getBrand().getId()!=0){
               sql+=(isMoreThanOneFieldToUpdate)?",":"";
               sql+=" brand_id=" + ((Toy) good).getBrand().getId();
               isMoreThanOneFieldToUpdate = true;
           }
           if(((Toy) good).getAnimalType() != null && ((Toy) good).getBrand().getId() != 0){
               sql+=(isMoreThanOneFieldToUpdate)?",":"";
               sql+=" animal_type_id=" + ((Toy) good).getAnimalType().getId();
           }
       }else if(good instanceof Filler){
           if(((Filler) good).getWeight()!=0){
               sql+=(isMoreThanOneFieldToUpdate)?",":"";
               sql+=" weight=" + ((Filler) good).getWeight();
               isMoreThanOneFieldToUpdate = true;
           }
           if(((Filler) good).getBrand()!=null&&((Filler) good).getBrand().getId()!=0){
               sql+=(isMoreThanOneFieldToUpdate)?",":"";
               sql+=" brand_id=" + ((Filler) good).getBrand().getId();
               isMoreThanOneFieldToUpdate = true;
           }
           if(((Filler) good).getAnimalType() != null && ((Filler) good).getBrand().getId() != 0){
               sql+=(isMoreThanOneFieldToUpdate)?",":"";
               sql+=" animal_type_id=" + ((Filler) good).getAnimalType().getId();
           }
       }else if(good instanceof Animal){
           if(((Animal) good).getBreed() != null && ((Animal) good).getBreed().getId() != 0){
               sql+=(isMoreThanOneFieldToUpdate)?",":"";
               sql+=" breed_id=" + ((Animal) good).getBreed().getId();
               isMoreThanOneFieldToUpdate = true;
           }
           if(((Animal) good).getAge() != 0){
               sql+=(isMoreThanOneFieldToUpdate)?",":"";
               sql+=" age=" + ((Animal) good).getAge();
               isMoreThanOneFieldToUpdate = true;
           }
           if(((Animal) good).getSex()!=0){
               sql+=(isMoreThanOneFieldToUpdate)?",":"";
               sql+=" sex=\'" + ((Animal) good).getSex() + "\'";
           }
       }

        return sql;

    }

    //Теперь совсем страшно жить
    private String getGoodStatementBasedOnCategory(String categoryID, String sql){

        final int i = Integer.parseInt(categoryID);
        String statement;

        switch(i){
            case FOOD: case FILLERS:
                statement = "SELECT g.id, g.name, g.brand_id, g.category_id, b.name as `brand`, `type_id`, t.name as `type`, `animal_type_id`, at.name as `animal_type`," +
                        "`weight`, `description`, `count`, `price`, `url` FROM goods g " +
                        "LEFT JOIN brands b on g.brand_id = b.id " +
                        "LEFT JOIN animal_types at on g.animal_type_id = at.id " +
                        "LEFT JOIN types t on g.type_id = t.id" +
                        sql;
                break;
            case CARE: case TOYS:
                statement = "SELECT g.id, g.name, g.brand_id, b.name as `brand`, g.category_id, `type_id`, t.name as `type`, `animal_type_id`, at.name as `animal_type`," +
                        "`description`, `count`, `price`, `url` FROM goods g " +
                        "LEFT JOIN brands b on g.brand_id = b.id " +
                        "LEFT JOIN animal_types at on g.animal_type_id = at.id " +
                        "LEFT JOIN types t on g.type_id = t.id" +
                        sql;
                break;
            case CAGES:
                statement = "SELECT g.id, g.name, `brand_id`, g.category_id, b.name as `brand`, `type_id`, t.name as `type`," +
                        "`length`, `width`, `height`, `description`, `count`, `price`, `url` FROM goods g " +
                        "LEFT JOIN brands b on g.brand_id = b.id " +
                        "LEFT JOIN types t on g.type_id = t.id" +
                        sql;
                break;
            case ANIMALS:
                statement = "SELECT g.id, g.name, g.category_id, `type_id`, t.name as `type`, " +
                        "`breed_id`, b.name as `breed`, `sex`, `age`, `description`, `count`, `price`, `url` FROM goods g " +
                        "LEFT JOIN types t on g.type_id = t.id " +
                        "LEFT JOIN breeds b on g.breed_id = b.id" +
                        sql;
                break;
            default:
                System.err.println("Неизвестная категория товара!");
                return null;
        }

        return statement;

    }


}
