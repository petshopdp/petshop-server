package data.dao.jdbc;

import data.dao.BasketDAO;
import data.entities.*;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BasketDAOImpl implements BasketDAO {

    private Logger logger = Logger.getLogger(BasketDAOImpl.class);

    @Override
    public List<BasketItem> find(int userID){

        Connection connection = null;

        try{

            connection = DBConnectionManager.getInstance().getConnection();

                    PreparedStatement psGoods = connection.prepareStatement("SELECT g.id, g.name, g.category_id, cat.name as category, g.type_id, t.name as 'type'," +
                    "g.description, g.count, g.price, g.url, bas.count as basket_count FROM goods g " +
                    "LEFT JOIN brands b ON g.brand_id = b.id " +
                    "LEFT JOIN categories cat ON g.category_id = cat.id " +
                    "LEFT JOIN types t ON  g.type_id = t.id " +
                    "LEFT JOIN baskets bas ON g.id = bas.good_id " +
                    "WHERE bas.user_id=? AND g.id IN " +
                    "(SELECT good_id FROM baskets WHERE user_id=?)");

            psGoods.setInt(1,userID);
            psGoods.setInt(2,userID);

            ResultSet rs = psGoods.executeQuery();

            return toEntity(rs);

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(), e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return null;
    }

    @Override
    public boolean create(BasketItem basketItem){

        Connection connection = null;

        try {

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psCheck = connection.prepareStatement("SELECT * FROM baskets WHERE user_id=? AND good_id=?;");
            psCheck.setInt(1,basketItem.getUserID());
            psCheck.setInt(2,basketItem.getGoodID());

            ResultSet rsCheck = psCheck.executeQuery();

            if(rsCheck.next()){

                PreparedStatement countUpdate = connection.prepareStatement("UPDATE baskets SET `count`=`count`+? WHERE user_id=? AND good_id=?;");
                countUpdate.setInt(1,basketItem.getCount());
                countUpdate.setInt(2,basketItem.getUserID());
                countUpdate.setInt(3,basketItem.getGoodID());

                countUpdate.executeUpdate();

                return true;

            }else {

                PreparedStatement psItem = connection.prepareStatement("INSERT INTO baskets(user_id,good_id,`count`) VALUES (?,?,?);");

                psItem.setInt(1, basketItem.getUserID());
                psItem.setInt(2, basketItem.getGoodID());
                psItem.setInt(3, basketItem.getCount());

                psItem.executeUpdate();

                return true;
            }

        } catch (SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e) {
            logger.error(e.getMessage(), e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return false;
    }

    @Override
    public BasketItem read(int goodID, int userID){

        Connection connection = null;

        try{

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psGoods = connection.prepareStatement("SELECT g.id, g.name, g.category_id, cat.name as category, g.type_id, t.name as 'type'," +
                    "g.description, g.count, g.price, g.url, bas.count as basket_count FROM goods g " +
                    "LEFT JOIN brands b ON g.brand_id = b.id " +
                    "LEFT JOIN categories cat ON g.category_id = cat.id " +
                    "LEFT JOIN types t ON  g.type_id = t.id " +
                    "LEFT JOIN baskets bas ON g.id = bas.good_id " +
                    "WHERE g.id IN " +
                    "(SELECT good_id FROM baskets WHERE user_id=? AND good_id=?)");

            psGoods.setInt(1,userID);
            psGoods.setInt(2,goodID);

            ResultSet rs = psGoods.executeQuery();

            return toEntity(rs).get(0);

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(), e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return null;
    }

    @Override
    public boolean update(BasketItem basketItem){

        Connection connection = null;

        try{

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psBasketItem = connection.prepareStatement("UPDATE baskets SET `count`=`count`+? WHERE user_id=? AND good_id=?;");

            psBasketItem.setInt(1,basketItem.getCount());
            psBasketItem.setInt(2,basketItem.getUserID());
            psBasketItem.setInt(3,basketItem.getGoodID());

            psBasketItem.executeUpdate();

            return true;

        }catch (SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(), e);
        }finally {
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return false;
    }

    @Override
    public boolean delete(int goodID, int userID){

        Connection connection = null;

        try {

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psGFromB = connection.prepareStatement(
                    "DELETE FROM baskets WHERE good_id=? AND user_id =?;");

            psGFromB.setInt(1, goodID);
            psGFromB.setInt(2,userID);

            psGFromB.executeUpdate();

            return true;
        } catch (SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e) {
            logger.error(e.getMessage(), e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }

        return false;
    }

    private List<BasketItem> toEntity(ResultSet rsBasket) throws SQLException {

        ArrayList<BasketItem> alBasketItems = new ArrayList<>();

        BasketItem basketItem;
        Good good;

        while(rsBasket.next()){

            good = new Good();

            good.setId(rsBasket.getInt("id"));
            good.setName(rsBasket.getString("name"));
            good.setType(new Type());
            good.getType().setId(rsBasket.getInt("type_id"));
            good.getType().setName(rsBasket.getString("type"));
            good.setCategoryID(rsBasket.getInt("category_id"));
            good.setDescription(rsBasket.getString("description"));
            good.setCount(rsBasket.getInt("count"));
            good.setPrice(rsBasket.getDouble("price"));
            good.setUrl(rsBasket.getString("url"));

            basketItem = new BasketItem();

            basketItem.setCount(rsBasket.getInt("basket_count"));
            basketItem.setGood(good);

            alBasketItems.add(basketItem);

        }

        return alBasketItems;

    }

}
