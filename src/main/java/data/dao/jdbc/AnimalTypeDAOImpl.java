package data.dao.jdbc;

import data.dao.AnimalTypeDAO;
import data.entities.AnimalType;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AnimalTypeDAOImpl extends AbstractDAO<AnimalType> implements AnimalTypeDAO {

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM animal_types ";
    }

    @Override
    protected String getUpdateQuery(AnimalType entity) {

        return "UPDATE animal_types SET name=\'" + entity.getName() + "\'";

    }

    @Override
    protected String getSelectForAllQuery() {
        return "SELECT * FROM animal_types";
    }

    @Override
    protected int getID(AnimalType entity) {
        return entity.getId();
    }

    @Override
    protected PreparedStatement getPreparedCreateStatement(Connection connection, AnimalType entity) throws SQLException {

        PreparedStatement psAnimalType = connection.prepareStatement("INSERT INTO animal_types(`name`) VALUE (?);");
        psAnimalType.setString(1,entity.getName());

        return psAnimalType;

    }

    @Override
    protected String getSelectQuery(Map<String, String[]> parameterMap) {

        String sql = "SELECT * FROM animal_types";
        boolean isMoreThanOneCondition = false;

        if(!parameterMap.isEmpty()) {

            sql+=" WHERE";

            if (parameterMap.containsKey("id")) {
                sql+=" id=" + parameterMap.get("id")[0];
                isMoreThanOneCondition = true;
            }
            if (parameterMap.containsKey("name")) {
                sql+=(isMoreThanOneCondition)?" AND":"";
                sql+=" name=\'" + parameterMap.get("name")[0] + "\'";
                isMoreThanOneCondition = true;
            }
            if(parameterMap.containsKey("category_id")){
                sql+=(isMoreThanOneCondition)?" AND":"";
                sql+=" id IN (SELECT animal_type_id FROM categories_for_animal_types WHERE category_id="
                     + parameterMap.get("category_id")[0] + ")";
            }
        }

        sql+=";";

        return sql;

    }

    @Override
    protected List<AnimalType> toEntity(ResultSet rs) throws SQLException {

        ArrayList<AnimalType> alAnimalTypes = new ArrayList<>();
        AnimalType animalType;

        while(rs.next()){

            animalType = new AnimalType();

            animalType.setId(rs.getInt("id"));
            animalType.setName(rs.getString("name"));

            alAnimalTypes.add(animalType);

        }
        return alAnimalTypes;
    }
}
