package data.dao.jdbc;

import data.dao.BrandDAO;
import data.entities.Brand;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BrandDAOImpl extends AbstractDAO<Brand> implements BrandDAO {


    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM brands";
    }

    @Override
    protected String getUpdateQuery(Brand entity) {

        String sql = "UPDATE brands SET";
        boolean isMoreThanOneFieldToUpdate = false;

        if(entity!=null && entity.getId() != 0){
            sql+=" id=\'" + entity.getId() + "\'";
            isMoreThanOneFieldToUpdate = true;
        }

        if(entity!=null && !entity.getName().isEmpty()){
            sql+=(isMoreThanOneFieldToUpdate)?",":"";
            sql+=" name=\'" + entity.getName() + "\'";
        }


        return sql;

    }

    @Override
    protected String getSelectForAllQuery() {
        return "SELECT * FROM brands";
    }

    @Override
    protected int getID(Brand entity) {
        return entity.getId();
    }

    @Override
    protected PreparedStatement getPreparedCreateStatement(Connection connection, Brand entity) throws SQLException {

        PreparedStatement psBrands = connection.prepareStatement("INSERT INTO brands(`name`) VALUES (?)");
        psBrands.setString(1,entity.getName());

        return psBrands;
    }

    @Override
    protected String getSelectQuery(Map<String, String[]> parameterMap) {

        String sql = "SELECT * FROM brands ";
        boolean isMoreThanOneCondition = false;

        if(!parameterMap.isEmpty()) {

            sql += "WHERE";

            if (parameterMap.containsKey("id")) {
                sql += " id=" + parameterMap.get("id")[0];
                isMoreThanOneCondition = true;
            }
            if (parameterMap.containsKey("name")) {
                sql += (isMoreThanOneCondition) ? " AND" : "";
                sql += " name=\'" + parameterMap.get("name")[0] + "\'";
                isMoreThanOneCondition = true;
            }
            if (parameterMap.containsKey("category_id")) {
                sql += (isMoreThanOneCondition) ? " AND" : "";
                sql += " id IN (SELECT brand_id FROM goods WHERE category_id="
                        + parameterMap.get("category_id")[0] + ");";
            }
        }

            sql+=";";

            return sql;

    }

    @Override
    protected List<Brand> toEntity(ResultSet rs) throws SQLException {

        ArrayList<Brand> alTypes = new ArrayList<>();

        Brand brand;

        while(rs.next()){

            brand = new Brand();

            brand.setId(rs.getInt("id"));
            brand.setName(rs.getString("name"));

            alTypes.add(brand);

        }

        return alTypes;
    }
}
