package data.dao.jdbc;

import org.apache.log4j.Logger;

import java.sql.*;
import java.util.List;
import java.util.Map;

public abstract class AbstractDAO<T> {

    private Logger logger = Logger.getLogger(AbstractDAO.class);

    protected abstract String getDeleteQuery();
    protected abstract String getUpdateQuery(T entity);
    protected abstract String getSelectForAllQuery();
    protected abstract int getID(T entity);
    protected abstract PreparedStatement getPreparedCreateStatement(Connection connection, T entity) throws SQLException;
    protected abstract String getSelectQuery(Map<String, String[]> parameterMap);
    protected abstract List<T> toEntity(ResultSet rs) throws SQLException;

    public boolean create(T entity){

      Connection connection = null;

      try {

          connection = DBConnectionManager.getInstance().getConnection();

          PreparedStatement ps = getPreparedCreateStatement(connection, entity);

          ps.executeUpdate();

          return true;

      }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){

          e.printStackTrace();
      }finally {
          if(connection!=null){
              try {
                  connection.close();
              } catch (SQLException e) {
                  logger.error(e.getMessage(), e);
              }
          }
      }
      return false;
    }

    public T read(int id){

        Connection connection = null;

        try{

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement ps = connection.prepareStatement(getSelectForAllQuery() + " WHERE id=?;");
            ps.setInt(1,id);

            ResultSet rs = ps.executeQuery();

            return toEntity(rs).get(0);

        }catch (SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(), e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return null;
    }

    public boolean update(T entity){

        Connection connection = null;

        try{

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement ps = connection.prepareStatement(getUpdateQuery(entity) + " WHERE id=?;");
            ps.setInt(1,getID(entity));

            int rowsAffected = ps.executeUpdate();

            return rowsAffected > 0;

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(),e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(),e);
                }
            }
        }
        return false;
    }

    public boolean delete(int id){

        Connection connection = null;

        try{
            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement ps = connection.prepareStatement(getDeleteQuery() + " WHERE id=?;");
            ps.setString(1,String.valueOf(id));

            int rowsAffected = ps.executeUpdate();

            return rowsAffected > 0;

        }catch (SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(),e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(),e);
                }
            }
        }
        return false;
    }

    public List<T> find(Map<String, String[]> parameterMap){

        Connection connection = null;

        try{
            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement ps = connection.prepareStatement(getSelectQuery(parameterMap));

            ResultSet rs = ps.executeQuery();

            return toEntity(rs);

        }catch (SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(),e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(),e);
                }
            }
        }
     return null;
    }

}
