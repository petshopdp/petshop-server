package data.dao.jdbc;


import data.dao.CommentResponseDAO;
import data.entities.CommentResponse;
import data.entities.User;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CommentResponseDAOImpl implements CommentResponseDAO {

    Logger logger = Logger.getLogger(CommentResponseDAOImpl.class);

    @Override
    public boolean create(CommentResponse commentResponse){

        Connection connection = null;

        try{

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psResponse = connection.prepareStatement("INSERT INTO responses(response_text,user_id,comment_id) VALUES (?,?,?);");

            psResponse.setString(1,commentResponse.getResponseText());
            psResponse.setInt(2,commentResponse.getUser().getId());
            psResponse.setInt(3,commentResponse.getCommentId());

            psResponse.executeUpdate();

            PreparedStatement psAmount = connection.prepareStatement("UPDATE comments SET amount_of_responses=amount_of_responses+1 WHERE id=?;");
            psAmount.setInt(1,commentResponse.getCommentId());

            psAmount.executeUpdate();

            return true;

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(),e);
        }finally {
            if(connection!=null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(),e);
                }
            }
        }
        return false;
    }

    @Override
    public CommentResponse read(int id){

        Connection connection = null;

        try{

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psResponse = connection.prepareStatement("SELECT r.id, r.response_text, r.user_id, u.username, u.first_name, u.last_name, comment_id FROM responses r" +
                                                                            "LEFT JOIN users u ON u.id=r.user_id WHERE r.id=?;");
            psResponse.setInt(1,id);

            ResultSet rsResponse = psResponse.executeQuery();

            return toEntity(rsResponse).get(0);

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(),e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(),e);
                }
            }
        }
        return null;
    }

    @Override
    public boolean update(int id, String responseText){

        Connection connection = null;

        try{

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psResponse = connection.prepareStatement("UPDATE responses SET response_text=?, updated=1 WHERE id=?;");

            psResponse.setString(1,responseText);
            psResponse.setInt(2,id);

            int rowsAffected = psResponse.executeUpdate();

            return rowsAffected > 0;

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(),e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(),e);
                }
            }
        }
    return false;
    }

    @Override
    public boolean delete(int id){

        Connection connection = null;

        try{

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psResponses = connection.prepareStatement("DELETE FROM responses WHERE id=?;");
            psResponses.setInt(1,id);

            int rowsAffected = psResponses.executeUpdate();

            return rowsAffected > 0;

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(),e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(),e);
                }
            }
        }
        return false;
    }

    @Override
    public List<CommentResponse> find(Map<String, String[]> parameterMap){

        Connection connection = null;

        try{
            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psResponses = connection.prepareStatement(getSelectQuery(parameterMap));

            ResultSet rsResponses = psResponses.executeQuery();

            return toEntity(rsResponses);

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(),e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(),e);
                }
            }
        }
        return null;
    }

    private String getSelectQuery(Map<String,String[]> parameterMap){

        String sql = "SELECT r.id, response_text, r.user_id, u.username, u.first_name, u.last_name, comment_id FROM responses r " +
                     "LEFT JOIN users u ON u.id=r.user_id";
        boolean isMoreThanOneCondition = false;

        if(!parameterMap.isEmpty()) {

            sql+=" WHERE";

            if (parameterMap.containsKey("id")) {
                sql += " r.id=" + parameterMap.get("id")[0];
                isMoreThanOneCondition=true;
            }
            if (parameterMap.containsKey("response_text")) {
                sql+=(isMoreThanOneCondition)?" AND":"";
                sql += " r.response_text=" + parameterMap.get("response_text")[0];
                isMoreThanOneCondition=true;
            }
            if (parameterMap.containsKey("user_id")) {
                sql+=(isMoreThanOneCondition)?" AND":"";
                sql += " r.user_id=" + parameterMap.get("user_id")[0];
                isMoreThanOneCondition=true;
            }
            if (parameterMap.containsKey("comment_id")) {
                sql+=(isMoreThanOneCondition)?" AND":"";
                sql += " r.comment_id=" + parameterMap.get("comment_id")[0];
            }
        }

        sql+=";";

        return sql;

    }

    private List<CommentResponse> toEntity(ResultSet rs) throws SQLException {

        List<CommentResponse> alResponses = new ArrayList<>();

        CommentResponse response;

        while(rs.next()){

            response = new CommentResponse();

            response.setId(rs.getInt("user_id"));
            response.setResponseText(rs.getString("response_text"));

            User user = new User();

            user.setId(rs.getInt("id"));
            user.setUsername(rs.getString("username"));
            user.setFirstName(rs.getString("first_name"));
            user.setLastName(rs.getString("last_name"));

            response.setUser(user);
            response.setCommentId(rs.getInt("comment_id"));

            alResponses.add(response);

        }

        return alResponses;

    }

}
