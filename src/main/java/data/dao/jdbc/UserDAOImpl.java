package data.dao.jdbc;

import data.dao.UserDAO;
import data.entities.User;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UserDAOImpl extends AbstractDAO<User> implements UserDAO {

    private Logger logger = Logger.getLogger(UserDAOImpl.class);

    @Override
    public boolean delete(int id) {

        Connection connection = null;

        try{

            connection = DBConnectionManager.getInstance().getConnection();

            connection.setAutoCommit(false);

            PreparedStatement psUser = connection.prepareStatement("DELETE FROM users WHERE id=?;");
            psUser.setInt(1, id);

            int rowsAffected = psUser.executeUpdate();

            PreparedStatement psAuthroriseData = connection.prepareStatement("DELETE FROM users_authorised WHERE user_id=?;");
            psAuthroriseData.setInt(1,id);
            psAuthroriseData.executeUpdate();

            PreparedStatement psBasket = connection.prepareStatement("DELETE FROM baskets WHERE user_id=?;");
            psBasket.setInt(1, id);
            psBasket.executeUpdate();

            connection.commit();
            connection.setAutoCommit(true);

            return rowsAffected > 0;

        }catch (SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(), e);
            try {
                connection.rollback();
            } catch (SQLException ex) {
                logger.error(e.getMessage(), e);
            }
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return false;
    }

    protected String getSelectForAllQuery(){
        return "SELECT * FROM users ";
    }

    @Override
    protected String getDeleteQuery() {
        return null;
    }

    @Override
    protected int getID(User entity) {
        return entity.getId();
    }

    @Override
    protected PreparedStatement getPreparedCreateStatement(Connection connection, User user) throws SQLException {

        PreparedStatement psUser = connection.prepareStatement("INSERT INTO users(username, password, first_name, last_name, email)" +
                                                                    " VALUES (?,?,?,?,?)");
        psUser.setString(1,user.getUsername());
        psUser.setString(2,user.getPassword());
        psUser.setString(3,user.getFirstName());
        psUser.setString(4,user.getLastName());
        psUser.setString(5,user.getEmail());

        return psUser;

    }


    protected String getSelectQuery(Map<String, String[]> parameterMap){

        String sql = "SELECT id, username, password, first_name, last_name, email FROM users";
        boolean isMoreThanOneCondition = false;

        if(!parameterMap.isEmpty()){

            sql+=" WHERE";

            if(parameterMap.containsKey("id")){
                sql+=" id=" + parameterMap.get("id")[0];
                isMoreThanOneCondition = true;
            }

            if(parameterMap.containsKey("username")){
                sql+=(isMoreThanOneCondition)?" AND":"";
                sql+=" username=\'" + parameterMap.get("username")[0] + "\'";
                isMoreThanOneCondition = true;
            }

            if(parameterMap.containsKey("password")){
                sql+=(isMoreThanOneCondition)?" AND":"";
                sql+=" password=\'" + parameterMap.get("password")[0] + "\'";
                isMoreThanOneCondition = true;
            }

            if(parameterMap.containsKey("first_name")){
                sql+=(isMoreThanOneCondition)?" AND":"";
                sql+=" frist_name=\'" + parameterMap.get("first_name")[0] + "\'";
                isMoreThanOneCondition = true;
            }

            if(parameterMap.containsKey("last_name")){
                sql+=(isMoreThanOneCondition)?" AND":"";
                sql+=" last_name=\'" + parameterMap.get("last_name")[0] + "\'";
                isMoreThanOneCondition = true;
            }

            if(parameterMap.containsKey("email")){
                sql+=(isMoreThanOneCondition)?" AND":"";
                sql+=" email=\'" + parameterMap.get("email")[0] + "\'";
            }

        }

        sql+=";";

        return sql;

    }

    protected String getUpdateQuery(User user){

        String sql = "UPDATE users SET";
        boolean isMoreThanOneField = false;

        if(user.getPassword()!=null && !user.getPassword().isEmpty()){
            sql+=" password=\'" + user.getPassword() + "\'";
            isMoreThanOneField = true;
        }

        if(user.getFirstName()!=null && !user.getFirstName().isEmpty()){
            sql+=(isMoreThanOneField)?",":"";
            sql+=" first_name=\'" + user.getFirstName() + "\'";
            isMoreThanOneField = true;
        }

        if(user.getLastName()!=null && !user.getLastName().isEmpty()){
            sql+=(isMoreThanOneField)?",":"";
            sql+=" last_name=\'" + user.getLastName() + "\'";
            isMoreThanOneField = true;
        }

        if(user.getEmail()!=null && !user.getEmail().isEmpty()){
            sql+=(isMoreThanOneField)?",":"";
            sql+=" email=\'" + user.getEmail() + "\'";
        }

        return sql;

    }

    protected List<User> toEntity(ResultSet rsUser) throws SQLException {

        ArrayList<User> alUsers = new ArrayList<>();

        User user;

        while(rsUser.next()){

            user = new User();

            user.setId(rsUser.getInt("id"));
            user.setUsername(rsUser.getString("username"));
            user.setPassword(rsUser.getString("password"));
            user.setFirstName(rsUser.getString("first_name"));
            user.setLastName(rsUser.getString("last_name"));
            user.setEmail(rsUser.getString("email"));

            alUsers.add(user);

        }
        return alUsers;
    }

}
