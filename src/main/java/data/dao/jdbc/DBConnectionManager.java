package data.dao.jdbc;

import data.PropertyReader;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DBConnectionManager {

    private final String url = PropertyReader.getInstance().getProperty("url");
    private final String user = PropertyReader.getInstance().getProperty( "username");
    private final String password = PropertyReader.getInstance().getProperty("password");

    private static DBConnectionManager instance;

    public static DBConnectionManager getInstance() throws IllegalAccessException,
                                                    InstantiationException,
                                                    ClassNotFoundException {
        synchronized(DBConnectionManager.class) {
            if (instance == null) instance = new DBConnectionManager();
        }
        return instance;
    }

    private DBConnectionManager() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        // загружаем драйвер один раз - при создании объекта DBConnection
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        //Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
    }

    public String getUrl() {
        return url;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public Connection getConnection() throws SQLException {
         return DriverManager.getConnection(url, user, password);
    }
}
