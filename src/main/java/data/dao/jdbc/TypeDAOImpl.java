package data.dao.jdbc;

import data.dao.TypeDAO;
import data.entities.Category;
import data.entities.Type;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TypeDAOImpl extends AbstractDAO<Type> implements TypeDAO {

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM types ";
    }

    @Override
    protected String getUpdateQuery(Type entity) {

        String sql = "UPDATE types SET";
        boolean isMoreThanOneFieldToUpdate = false;

        if(entity.getName()!=null && !entity.getName().isEmpty()){
            sql+=" name=\'" + entity.getName() + "\'";
            isMoreThanOneFieldToUpdate = true;
        }

        if(entity.getCategory().getId()!=0){
            sql+=(isMoreThanOneFieldToUpdate)?",":"";
            sql+=" category_id=" + entity.getCategory().getId();
        }

        return sql;

    }

    @Override
    protected String getSelectForAllQuery() {
        return "SELECT t.id,t.name,category_id, cat.id, cat.name as category FROM types t JOIN categories cat on t.category_id = cat.id ";
    }

    @Override
    protected int getID(Type entity) {
        return entity.getId();
    }

    @Override
    protected PreparedStatement getPreparedCreateStatement(Connection connection, Type entity) throws SQLException {

        PreparedStatement psType = connection.prepareStatement("INSERT INTO types(id,`name`, category_id) VALUES (?,?,?);");

        psType.setString(1,String.valueOf(entity.getId()));
        psType.setString(2,entity.getName());
        psType.setString(3,String.valueOf(entity.getCategory().getId()));

        return psType;

    }

    @Override
    protected String getSelectQuery(Map<String, String[]> parameterMap) {

       String sql = "SELECT t.id,t.name,category_id, cat.name as category, cat.url as category_url FROM types t JOIN categories cat on t.category_id = cat.id";
       boolean isMoreThanOneCondition = false;

       if(!parameterMap.isEmpty()){

           sql+=" WHERE";

           if(parameterMap.containsKey("id")){
               sql+=" t.id=" + parameterMap.get("id")[0];
               isMoreThanOneCondition = true;
           }
           if(parameterMap.containsKey("name")){
               sql+=(isMoreThanOneCondition)?" AND":"";
               sql+=" t.name=\'" + parameterMap.get("name")[0] + "\'";
               isMoreThanOneCondition = true;
           }
           if(parameterMap.containsKey("category_id")){
               sql+=(isMoreThanOneCondition)?" AND":"";
               sql+=" t.category_id=" + parameterMap.get("category_id")[0];
           }

       }

       sql+=";";

        System.out.println(sql);

       return sql;

    }

    @Override
    protected List<Type> toEntity(ResultSet rs) throws SQLException {

        ArrayList<Type> alTypes = new ArrayList<>();

        Type type;

        while(rs.next()){

            type = new Type();

            type.setId(rs.getInt("id"));
            type.setName(rs.getString("name"));
            type.setCategory(new Category(rs.getInt("category_id"),
                             rs.getString("category"),rs.getString("category_url")));

            alTypes.add(type);

        }

        return alTypes;

    }
}
