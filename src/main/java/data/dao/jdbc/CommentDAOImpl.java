package data.dao.jdbc;

import data.dao.CommentDAO;
import data.entities.Comment;
import data.entities.User;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CommentDAOImpl implements CommentDAO {

    private Logger logger = Logger.getLogger(CommentDAOImpl.class);

    @Override
    public boolean create(Comment comment){

        Connection connection = null;

        try{

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psComment = connection.prepareStatement("INSERT INTO comments(comment_text, user_id, good_id) VALUES (?,?,?);");

            psComment.setString(1,comment.getCommentText());
            psComment.setInt(2,comment.getUser().getId());
            psComment.setInt(3,comment.getGoodID());

            psComment.executeUpdate();

            return true;

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(),e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(),e);
                }
            }
        }
        return false;
    }

    @Override
    public Comment read(int id){

        Connection connection = null;

        try{
            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psComment = connection.prepareStatement("SELECT c.id, c.comment_text, c.user_id, u.username, u.first_name, u.last_name, c.good_id," +
                                                                            "amount_of_responses, c.updated FROM comments c"
                                                                            + " LEFT JOIN users u ON c.user_id=u.id " +
                                                                            "WHERE id=?;");
            psComment.setInt(1,id);

            ResultSet rsComment = psComment.executeQuery();

            return toEntity(rsComment).get(0);

        }catch (SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(),e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(),e);
                }
            }
        }
        return null;
    }

    @Override
    public boolean update(int id, String commentText){

        Connection connection = null;

        try{

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psComment = connection.prepareStatement("UPDATE comments SET comment_text=?, updated=1 WHERE id=?;");

            psComment.setString(1,commentText);
            psComment.setInt(2,id);

            psComment.executeUpdate();

            return true;

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(), e);
        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(),e);
                }
            }
        }
        return false;
    }

    @Override
    public boolean delete(int id){

        Connection connection = null;

        try{

            connection = DBConnectionManager.getInstance().getConnection();

            connection.setAutoCommit(false);

            PreparedStatement psComment = connection.prepareStatement("DELETE FROM comments WHERE id=?;");
            psComment.setInt(1,id);

            int rowsAffected = psComment.executeUpdate();

            PreparedStatement psResponses = connection.prepareStatement("DELETE FROM responses WHERE comment_id=?;");
            psResponses.setInt(1,id);

            psResponses.executeUpdate();

            connection.commit();
            connection.setAutoCommit(true);

            return  rowsAffected>0;

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(),e);

             try{
                    if(connection!=null) connection.rollback();
                } catch (SQLException ex) {
                    logger.error(e.getMessage(), e);
                }

        }finally {
            if(connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(),e);
                }
            }
        }
     return false;
    }

    @Override
    public List<Comment> find(Map<String, String[]> parameterMap){

        Connection connection = null;

        try{
            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psComments = connection.prepareStatement(getSelectQuery(parameterMap));

            ResultSet rsComments = psComments.executeQuery();

            return toEntity(rsComments);

        }catch (SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
        logger.error(e.getMessage(),e);
        }finally{
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e.getMessage(),e);
                }
            }
        }
        return null;
    }

    private String getSelectQuery(Map<String,String[]> parameterMap){

        String sql = "SELECT c.id, c.comment_text, c.user_id, u.username, u.first_name, u.last_name, c.good_id,"
                    + "amount_of_responses, c.updated FROM comments c"
                    + " LEFT JOIN users u ON c.user_id=u.id";

        boolean isMoreThanOneCondition = false;

        if(!parameterMap.isEmpty()) {

            sql+=" WHERE";

            if (parameterMap.containsKey("id")) {
                sql += " c.id=" + parameterMap.get("id")[0];
                isMoreThanOneCondition=true;
            }
            if (parameterMap.containsKey("comment_text")) {
                sql+=(isMoreThanOneCondition)?" AND":"";
                sql += " c.comment_text=" + parameterMap.get("comment_text")[0];
                isMoreThanOneCondition=true;
            }
            if (parameterMap.containsKey("user_id")) {
                sql+=(isMoreThanOneCondition)?" AND":"";
                sql += " c.user_id=" + parameterMap.get("user_id")[0];
                isMoreThanOneCondition=true;
            }
            if (parameterMap.containsKey("good_id")) {
                sql+=(isMoreThanOneCondition)?" AND":"";
                sql += " c.good_id=" + parameterMap.get("good_id")[0];
            }
        }

        sql+=";";

        return sql;

    }

    private List<Comment> toEntity(ResultSet rs) throws SQLException {

        List<Comment> alComments = new ArrayList<>();

        Comment comment;

        while (rs.next()) {

            comment = new Comment();

            comment.setId(rs.getInt("id"));
            comment.setCommentText(rs.getString("comment_text"));


            User user = new User();

            user.setId(rs.getInt("user_id"));
            user.setUsername(rs.getString("username"));
            user.setFirstName(rs.getString("first_name"));
            user.setLastName(rs.getString("last_name"));

            comment.setUser(user);


            comment.setGoodID(rs.getInt("good_id"));

            if(rs.getInt("amount_of_responses") > 0){
                comment.setHasResponses(true);
            }else{
                comment.setHasResponses(false);
            }

            comment.setAmountOfResponses(rs.getInt("amount_of_responses"));
            comment.setUpdated(rs.getInt("updated") > 0);

            alComments.add(comment);

        }

        return alComments;

    }

}
