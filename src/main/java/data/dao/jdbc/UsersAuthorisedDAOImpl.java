package data.dao.jdbc;

import data.dao.UsersAuthorisedDAO;
import data.entities.UsersAuthorised;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UsersAuthorisedDAOImpl implements UsersAuthorisedDAO {

    private Logger logger = Logger.getLogger(UsersAuthorisedDAOImpl.class);

    @Override
    public boolean create(UsersAuthorised usersAuthorised) {

        Connection connection;

        try{

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psUsersAuthorised = connection.prepareStatement(
                    "INSERT INTO users_authorised(user_id,device_tag,is_authorised) VALUES (?,?,?)");

            psUsersAuthorised.setInt(1,usersAuthorised.getUserID());
            psUsersAuthorised.setString(2,usersAuthorised.getDeviceTag());
            psUsersAuthorised.setInt(3, usersAuthorised.isAuthorised());

            psUsersAuthorised.executeUpdate();

            return true;

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(), e);
        }
        return false;
    }

    @Override
    public UsersAuthorised read(int user_id, String deviceTag) {

        Connection connection;

        try{

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psUsersAuthorised = connection.prepareStatement("SELECT * FROM users_authorised " +
                                                                                  "WHERE user_id=? AND device_tag=?;");

            psUsersAuthorised.setInt(1,user_id);
            psUsersAuthorised.setString(2,deviceTag);

            ResultSet rsUsersAuthorised = psUsersAuthorised.executeQuery();

            return toEntity(rsUsersAuthorised).get(0);

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public boolean update(UsersAuthorised usersAuthorised) {

        Connection connection;

        try{
            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psUsersAuthorised = connection.prepareStatement(getUpdateQuery(usersAuthorised));

            int rowsAffected = psUsersAuthorised.executeUpdate();

            return rowsAffected > 0;

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(), e);
        }
        return false;
    }

    @Override
    public List<UsersAuthorised> find(Map<String, String[]> parameterMap) {

        Connection connection;

        try{

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psUsersAuthorised = connection.prepareStatement(getSelectQuery(parameterMap));

            ResultSet rsUsersAuthorised = psUsersAuthorised.executeQuery();

            return toEntity(rsUsersAuthorised);

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public boolean delete(int userID) {

        Connection connection;

        try{

            connection = DBConnectionManager.getInstance().getConnection();

            PreparedStatement psUsersAuthorised = connection.prepareStatement("DELETE FROM users_authorised WHERE userID=?;");
            psUsersAuthorised.setInt(1,userID);

            psUsersAuthorised.executeUpdate();

            return true;

        }catch(SQLException | IllegalAccessException | InstantiationException | ClassNotFoundException e){
            logger.error(e.getMessage(), e);
        }
        return false;
    }

    private List<UsersAuthorised> toEntity(ResultSet rsUsersAuthorised) throws SQLException {

        ArrayList<UsersAuthorised> alUsers = new ArrayList<>();

        UsersAuthorised usersAuthorised;

        while(rsUsersAuthorised.next()){

            usersAuthorised = new UsersAuthorised();

            usersAuthorised.setUserID(rsUsersAuthorised.getInt("user_id"));
            usersAuthorised.setDeviceTag(rsUsersAuthorised.getString("device_tag"));
            usersAuthorised.setAuthorised(rsUsersAuthorised.getInt("is_authorised"));

            alUsers.add(usersAuthorised);

        }

        return alUsers;

    }

    private String getUpdateQuery(UsersAuthorised usersAuthorised){

        String sql = "UPDATE users_authorised SET is_authorised=" + usersAuthorised.isAuthorised();
        boolean isMoreThanOneCondition=false;

        sql+=" WHERE";

        if(usersAuthorised.getUserID()!=0){
            sql+=" user_id=" + usersAuthorised.getUserID();
            isMoreThanOneCondition=true;
        }
        if(usersAuthorised.getDeviceTag()!=null && !usersAuthorised.getDeviceTag().isEmpty()){
            sql+=(isMoreThanOneCondition)?" AND":"";
            sql+=" device_tag=" + usersAuthorised.getDeviceTag();
        }

        sql+=";";

        return sql;

    }

    private String getSelectQuery(Map<String, String[]> parameterMap){

        String sql = "SELECT * FROM users_authorised";
        boolean isMoreThanOneCondition = false;

        if(!parameterMap.isEmpty()){

            sql+=" WHERE";

            if(parameterMap.containsKey("user_id")){
                sql+=" user_id=" + parameterMap.get("user_id")[0];
                isMoreThanOneCondition=true;
            }
            if(parameterMap.containsKey("device_tag")){
                sql+=(isMoreThanOneCondition)?" AND":"";
                sql+=" device_tag='" + parameterMap.get("device_tag")[0] + "'";
                isMoreThanOneCondition=true;
            }
            if(parameterMap.containsKey("is_authorised")){
                sql+=(isMoreThanOneCondition)?" AND":"";
                sql+=" is_authorised=" + parameterMap.get("is_authorised")[0];
            }
        }

        sql+=";";

        return sql;

    }


}
