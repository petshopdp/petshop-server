package data.dao;

import data.entities.Category;

import java.util.List;
import java.util.Map;

public interface CategoryDAO {

    boolean create(Category category);

    Category read(int id);

    List<Category> find(Map<String, String[]> parameterMap);

    boolean update(Category category);

    boolean delete(int id);

}
