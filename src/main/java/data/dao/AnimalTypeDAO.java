package data.dao;

import data.entities.AnimalType;

import java.util.List;
import java.util.Map;

public interface AnimalTypeDAO {

    boolean create(AnimalType animalType);

    AnimalType read(int id);

    List<AnimalType> find(Map<String, String[]> parameterMap);

    boolean update(AnimalType animalType);

    boolean delete(int id);

}
