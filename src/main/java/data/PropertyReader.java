package data;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyReader {

    private final String PATH_TO_PROPERTIES = "config.properties";
    private Properties prop = new Properties();

    private static PropertyReader instance = new PropertyReader();

    public static PropertyReader getInstance()
    {
        synchronized(PropertyReader.class) {
            if (instance == null) instance = new PropertyReader();
        }
        return instance;
    }

    private PropertyReader(){
        try {

           ClassLoader loader = Thread.currentThread().getContextClassLoader();
           InputStream foo = loader.getResourceAsStream(PATH_TO_PROPERTIES);

           prop.load(foo);

        } catch (IOException e) {
            System.out.println("Property file " + PATH_TO_PROPERTIES + " not found.");
            e.printStackTrace();
        }

    }

    public String getProperty(String propertyName){
        return prop.getProperty(propertyName);
    }


}
