package data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Good {

    @JsonIgnore
    public static final String BASE_NAME = "goods";

    protected int id;
    protected String name;

    @JsonProperty("category_id")
    protected int categoryID;

    protected Type type;
    protected String description;
    protected int count;
    protected double price;
    protected String url;

    public Good() {}

    public Good(int id, String name, int categoryID, Type type, String description, int count, double price, String url) {
        this.id = id;
        this.name = name;
        this.categoryID = categoryID;
        this.type = type;
        this.description = description;
        this.count = count;
        this.price = price;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Good{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", categoryID=" + categoryID +
                ", type=" + type +
                ", description='" + description + '\'' +
                ", count=" + count +
                ", price=" + price +
                ", url='" + url + '\'' +
                '}';
    }
}
