package data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class UsersAuthorised {

    @JsonIgnore
    public static final String BASE_NAME = "users_authorised";

    private int userID;
    private String deviceTag;
    private int isAuthorised;

    public UsersAuthorised() {}

    public UsersAuthorised(int userID, String deviceTag, int isAuthorised) {
        this.userID = userID;
        this.deviceTag = deviceTag;
        this.isAuthorised = isAuthorised;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getDeviceTag() {
        return deviceTag;
    }

    public void setDeviceTag(String deviceTag) {
        this.deviceTag = deviceTag;
    }

    public int isAuthorised() {
        return isAuthorised;
    }

    public void setAuthorised(int authorised) {
        isAuthorised = authorised;
    }

    @Override
    public String toString() {
        return "UsersAuthorised{" +
                "userID=" + userID +
                ", deviceTag='" + deviceTag + '\'' +
                ", isAuthorised=" + isAuthorised +
                '}';
    }

}
