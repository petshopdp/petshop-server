package data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class AnimalType {

    @JsonIgnore
    public static final String BASE_NAME = "animal_types";

    private int id;
    private String name;

    public AnimalType(){}

    public AnimalType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "AnimalType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';

    }
}
