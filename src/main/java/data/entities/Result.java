package data.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Result {

    public static final String BASE_NAME = "message";

    public static final int OPERATION_SUCCESSFUL = 1;
    public static final int OPERATION_FAILED = 2;

    private int code;

    @JsonProperty("text")
    private String message;

    public Result() {}

    public Result(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "Result{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }

}
