package data.entities;

public class Cage extends Good {


    private Brand brand;
    private int length;
    private int width;
    private int height;

    public Cage() {}

    public Cage(int id, String name, Brand brand, Type type, int length, int width, int height, String description, int count, int price, String url) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.type = type;
        this.length = length;
        this.width = width;
        this.height = height;
        this.description = description;
        this.count = count;
        this.price = price;
        this.url = url;
    }


    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Cage{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", brand=" + brand +
                ", type=" + type +
                ", length=" + length +
                ", width=" + width +
                ", height=" + height +
                ", description='" + description + '\'' +
                ", count=" + count +
                ", price=" + price +
                ", url='" + url + '\'' +
                '}';
    }
}
