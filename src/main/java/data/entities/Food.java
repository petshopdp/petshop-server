package data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Food extends Good {

    private Brand brand;

    @JsonProperty("animal_type")
    private AnimalType animalType;

    private int weight;

    public Food() {}

    public Food(int id, String name, Brand brand, Type type, AnimalType animalType, int weight, String description, int count, double price, String url) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.type = type;
        this.animalType = animalType;
        this.weight = weight;
        this.description = description;
        this.count = count;
        this.price = price;
        this.url = url;
    }


    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public AnimalType getAnimalType() {
        return animalType;
    }

    public void setAnimalType(AnimalType animalType) {
        this.animalType = animalType;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }


    @Override
    public String toString() {
        return "Food{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", brand=" + brand +
                ", type=" + type +
                ", animalType=" + animalType +
                ", weight=" + weight +
                ", description='" + description + '\'' +
                ", count=" + count +
                ", price=" + price +
                ", url='" + url + '\'' +
                '}';
    }
}
