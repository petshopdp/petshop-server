package data.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Toy extends Good {

    private Brand brand;

    @JsonProperty("animal_type")
    private AnimalType animalType;

    public Toy() {}

    public Toy(int id, String name, Brand brand, Type type, AnimalType animalType, String description, int count, double price, String url) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.type = type;
        this.animalType = animalType;
        this.description = description;
        this.count = count;
        this.price = price;
        this.url = url;
    }


    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public AnimalType getAnimalType() {
        return animalType;
    }

    public void setAnimalType(AnimalType animalType) {
        this.animalType = animalType;
    }

    @Override
    public String toString() {
        return "Toy{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", brand=" + brand +
                ", type=" + type +
                ", animalType=" + animalType +
                ", description='" + description + '\'' +
                ", count=" + count +
                ", price=" + price +
                ", url='" + url + '\'' +
                '}';
    }

}
