package data.entities;


public class Animal extends Good {

    private Breed breed;
    private char sex = 0;
    private int age;

    public Animal() {}

    public Animal(int id, String name, Type type, Breed breed, char sex, int age, String description, int count, double price, String url) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.breed = breed;
        this.sex = sex;
        this.age = age;
        this.description = description;
        this.count = count;
        this.price = price;
        this.url = url;
    }

    public Breed getBreed() {
        return breed;
    }

    public void setBreed(Breed breed) {
        this.breed = breed;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", breed=" + breed +
                ", sex=" + sex +
                ", age=" + age +
                ", description='" + description + '\'' +
                ", count=" + count +
                ", price=" + price +
                ", url='" + url + '\'' +
                '}';
    }

}
