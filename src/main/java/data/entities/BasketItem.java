package data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties({"userID","goodID","BASE_NAME"})
public class BasketItem {

    public static final String BASE_NAME = "baskets";

    private int userID;
    private int goodID;

    @JsonIgnoreProperties({"description"})
    private Good good;

    @JsonProperty("count_in_basket")
    private int count;

    public BasketItem() {}

    public BasketItem(int userID, int goodID, int count, Good good) {
        this.userID = userID;
        this.goodID = goodID;
        this.count = count;
        this.good = good;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getGoodID() {
        return goodID;
    }

    public void setGoodID(int goodID) {
        this.goodID = goodID;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Good getGood() {
        return good;
    }

    public void setGood(Good good) {
        this.good = good;
    }

    @Override
    public String toString() {
        return "BasketItem{" +
                "userID=" + userID +
                ", goodID=" + goodID +
                ", good=" + good +
                ", count=" + count +
                '}';
    }
}
