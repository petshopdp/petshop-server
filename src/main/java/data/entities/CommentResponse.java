package data.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CommentResponse {

    public static final String BASE_NAME = "responses";

    private int id;

    @JsonProperty("response_text")
    private String responseText;

    @JsonIgnoreProperties({"first_name","last_name","email"})
    private User user;

    private int commentId;

    private boolean updated;

    public CommentResponse() {}

    public CommentResponse(int id, String responseText, User user, int commentId, boolean updated) {
        this.id = id;
        this.responseText = responseText;
        this.user = user;
        this.commentId = commentId;
        this.updated = updated;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    @Override
    public String toString() {
        return "CommentResponse{" +
                "id=" + id +
                ", responseText='" + responseText + '\'' +
                ", user=" + user +
                ", commentId=" + commentId +
                ", updated=" + updated +
                '}';
    }
}
