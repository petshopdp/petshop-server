package data.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Comment {

    public static final String BASE_NAME = "comments";

    private int id;

   @JsonProperty("comment_text")
   private String commentText;

   @JsonIgnoreProperties({"email"})
   private User user;

   private boolean updated;

   @JsonProperty("good_id")
   private int goodID;

   @JsonProperty("amount_of_Responses")
   private int amountOfResponses;

   @JsonProperty("has_responses")
   private boolean hasResponses;


    public Comment() {}

    public Comment(int id, String commentText, User user, boolean updated, int goodID, int amountOfResponses, boolean hasResponses) {
        this.id = id;
        this.commentText = commentText;
        this.user = user;
        this.updated = updated;
        this.goodID = goodID;
        this.amountOfResponses = amountOfResponses;
        this.hasResponses = hasResponses;
    }

    public int getGoodID() {
        return goodID;
    }

    public void setGoodID(int goodID) {
        this.goodID = goodID;
    }

    public int getAmountOfResponses() {
        return amountOfResponses;
    }

    public void setAmountOfResponses(int amountOfResponses) {
        this.amountOfResponses = amountOfResponses;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public void setHasResponses(boolean hasResponses) {
        this.hasResponses = hasResponses;
    }

    public boolean hasResponses() {
        return hasResponses;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", commentText='" + commentText + '\'' +
                ", user=" + user +
                ", updated=" + updated +
                ", goodID=" + goodID +
                ", amountOfResponses=" + amountOfResponses +
                ", hasResponses=" + hasResponses +
                '}';
    }
}
