package data;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;

public class JSONConverter {

    public static String toJSON(String name,Object obj) {

        ObjectMapper objectMapper= new ObjectMapper();

        String jsonString = null;

        HashMap<String, Object> map = new HashMap<>();

        map.put(name, obj);

        try {
            jsonString = objectMapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            System.out.println("Unable to covert object to JSON. The reason is:\n");
            e.printStackTrace();
        }

        return jsonString;

    }

}
